# Simple Node File Server

## Basic Features and Usage:
- Allows you to spin up a server and make files / folders accessible via HTTP
- The idea is to quickly make files remotely accessible
- Lists directory contents if no file is specified
- Allows file downloads
- Configurable Port and Folder from package.json

## Install

`npm install`

## Start

`npm start`

## Have Fun!
