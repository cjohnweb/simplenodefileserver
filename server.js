var express = require('express');
var serveIndex = require('serve-index');
var path = require('path');
var pj = require('./package.json');

const HTTP_PORT = pj.configure.HTTP_PORT;
const RELATIVE_PATH = pj.configure.RELATIVE_PATH;
let PATH_TO_SERVER;

if (RELATIVE_PATH) {
  PATH_TO_SERVER = __dirname +"\\" + pj.configure.PATH_TO_SERVER;
} else {
  PATH_TO_SERVER = pj.configure.PATH_TO_SERVER;
}


var app = express(); // Initialize Express
app.disable('x-powered-by');

app.use(express.static(PATH_TO_SERVER))
app.use('/', serveIndex(PATH_TO_SERVER));

app.listen(HTTP_PORT, function () {
  console.log('\nSimple File Server by John Minton <cjohnweb@gmail.com>\n');
  console.log('HTTP_PORT: ' + HTTP_PORT);
  console.log('RELATIVE_PATH: ' + RELATIVE_PATH);
  console.log('Serving Files from ' + PATH_TO_SERVER);
  console.log(`\n\nServing Running! Go to http://localhost:${HTTP_PORT}/`);
});

module.exports = app;
